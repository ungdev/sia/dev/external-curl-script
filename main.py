#! /usr/local/bin/python3
# coding: utf-8

import requests
import logging
import json

webhook_url = ''


def post_message_to_slack(text):
    return requests.post(
        webhook_url, data=json.dumps({'text': text}),
        headers={'Content-Type': 'application/json'}
    )

logging.basicConfig(
    format='%(asctime)s.%(msecs)03d %(levelname)s %(message)s',
    datefmt='%Y-%m-%d,%H:%M:%S',
    level=logging.INFO
)


if __name__ == '__main__':

    logging.info("Script starting.")

    # set URL for request and execute it
    prometheusURL = 'https://prometheus.uttnetgroup.fr'
    logging.info("Requesting URL '" + prometheusURL + "'...")
    responseCURL = requests.get(prometheusURL)
    logging.info("Requesting done.")

    if (responseCURL.status_code != 200):
        logging.info("Status code is: \"" + str(responseCURL.status_code) + "\". Sending message to Slack.")
        slack_text = "SIA infrastructure is unreachable ! - <!subteam^TEZAEAZMEMZAMEZAM>"
        logging.info("Sending message to #servers-monitoring channel...")
        post_message_to_slack(slack_text)
        logging.info("Sending done.")

    else:
        logging.info("Status code is: \"" + str(responseCURL.status_code) + "\".")
        logging.info("Everything is fine. Nothing to do.")
